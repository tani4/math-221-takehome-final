class Airport:
    def __init__(self, code, name, lat, long):
        self.code = code
        self.name = name
        self.lat = float(lat)
        self.long = float(long)
        self.distances = []

    def __str__(self):
        return "Name: " + self.name + " Lat: " + self.lat + " Long: " + self.long

    def initializeDistances(self, airports):
        for airport in airports:
            dist = self.calculateDistance(airport)
            self.distances.append(dist)

    def calculateDistance(self, airport):
        delta_lat = self.lat - airport.lat
        delta_long = self.long - airport.long
        return (delta_lat**2 + delta_long**2)**0.5 * 111; #111: Convert from degrees to km

    def getDistance(self, to_node):
        return self.distances[to_node]