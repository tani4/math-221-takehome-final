import csv
from Airport import Airport
from DistanceMatrix import DistanceMatrix

def getDistanceMatrix():
    airports = []
    with open('airports.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            airport = Airport(row[0], row[2], row[3], row[4])
            airports.append(airport)
            #print(airport)
    distanceMatrix =  DistanceMatrix(airports)
    return distanceMatrix
