from Airport import  Airport

class DistanceMatrix:
    def __init__(self, airports):
        self.airports = airports
        for airport in airports:
            airport.initializeDistances(airports)

    def getDistance(self, from_node, to_node):
        return self.airports[from_node].getDistance(to_node)

    def getName(self, index):
        return self.airports[index].name

    def getAirport(self, index):
        return self.airports[index]

    def __len__(self):
        return len(self.airports)
