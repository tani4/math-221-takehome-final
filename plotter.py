import matplotlib.pyplot as plt
from Airport import Airport

def addAirport(airport, vehicle_number):
    plt.scatter(airport.long, airport.lat, color = getColor(vehicle_number))
    plt.annotate(processName(airport.name), (airport.long, airport.lat))

def processName(name):
    name = name.split(" ")
    output = ""
    for string in name:
        if string == "Airport":
            continue
        elif string == "International":
            output += "INTL "
        elif string == "Regional":
            output += "RGNL "
        else: output += string + " "
    return output

def getColor(vehicle_number):
    if vehicle_number == 0:
        return "red"
    elif vehicle_number == 1:
        return "blue"
    elif vehicle_number == 2:
        return "lawngreen"
    elif vehicle_number == 3:
        return "gold"
    elif vehicle_number == 4:
        return "magenta"
    else:
        return "cyan"

def show():
    plt.show()
